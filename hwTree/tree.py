import os.path
import sys


# input (argv): chip name, layers
# output (print): chip tree, parts list, total counts
def main():

	# usage
	if len(sys.argv) < 2 or len(sys.argv) > 3:
		print('Usage: python tree.py chip [layers]')
		exit(1)
		
	# get args	
	chip = sys.argv[1]
	if (len(sys.argv) > 2):
		layers = sys.argv[2]
	else: layers = 0
	
	# build chip tree
	t = ChipTree(chip, layers)
	t.buildChipTree(chip, 0)
	
	# print parts list, tree, and counts
	t.printAllPartsList()
	t.printChipTree(chip, 0, True)
	t.printCounts(chip)
	
	exit(0)
	

# returns full path name of chip.hdl	
def pathOf(chip):
	return 'chips/' + chip + '.hdl'

	
# Maps .hdl files to a chip hierarchy
class ChipTree:

	""" initialization """
	
	# setup roots of data structures
	def __init__(self, chip, layers):
		# components of each chip
		self.specs = {
			'Nand' : {},
			'Bit' : {},
			'Screen' : {},
			'Keyboard' : {},
			'ROM32K' : {},
			'ARegister' : {},
			'DRegister' : {},
			'RAM512' : {}
		}
		
		# num of nand gates and 1-bit registers in a chip
		self.counts = {
			# Built in chips
			'Nand' : [1,0],
			'Bit' : [0,1],
			'Screen' : [167535*16, 8192*16],
			'Keyboard' : [0,1],
			'ROM32K' : [167535*64, 8192*64],
			'ARegister' : [0,16],
			'DRegister' : [0,16],
			# save time traversing RAM hierarchy
			'RAM512' : [167535, 8192]
		}
		
		self.dups = []
		self.L = 255 if layers == 'i' else layers
		
		
	# recursively builds chip tree from chips components
	def buildChipTree(self, chip, layer):
	
		# ignore chips already known
		if self.hasSpecs(chip): return
		
		# pursue recursion if no specs on chip
		elif os.path.exists(pathOf(chip)):
		
			# add to duplicates, save dup status of this chip
			notInDups = chip not in self.specs
			if notInDups:
				self.specs[chip] = {}
				
			# recursion for each sub-chip
			chips = self.parseFile(chip)
			for c in chips:
			
				# record specs of chip
				if c not in self.specs[chip]:
				
					# add sub chip to list
					self.specs[chip].update({ c : 1 })
					
				elif notInDups:
				
					# increment sub chip count
					self.specs[chip][c] += 1
					
				self.buildChipTree(c, layer)
				
		# chip hdl file not found
		else:
			print('could not find: ' + pathOf(chip))
					
	
	""" printing """
	
	# prints parts list for all chips in tree
	def printAllPartsList(self):
		
		# title
		print
		print('Parts List:')
		
		# parts list
		for chip in self.specs:
			self.printPartsList(chip)
		
	# print parts list for a chip			
	def printPartsList(self, chip):
	
		# chip name
		print
		print '',
		print('-'+chip)
		
		# label given chips
		if len(self.specs[chip]) == 0:
			print('  - Given')
			return
			
		# parts	
		for part in self.specs[chip]:
			print('  -' + str(self.specs[chip][part]) + 'x ' + part)
		
		
	# print chip tree
	def printChipTree(self, chip, layer, printEnable):
	
		# title
		print
		print('Chip Tree: ')
		
		# recursively print tree
		self.printChipTreeR(chip, layer, printEnable)
		
			
	# print counts from getCounts
	def printCounts(self, chip):
	
		count = self.getCounts(chip)
		
		# title
		print
		print('Counts: ')
		
		# counts
		print('Total # of Nand gates: ' + str(count[0]))
		print('Total # of 1-Bit registers: ' + str(count[1]))
		print('Total # of 16-Bit registers: ' + str(count[1] / 16))
		
			
	""" helper methods """
			
	# returns list of chips from hdl file
	def parseFile(self, file):
	
		chips = []
	
		with open(pathOf(file)) as f:
			for line in f:
			
				# remove white space
				chip = line.strip()
				
				# remove comments
				chip = chip.split('//', 1)[0]
				chip = chip.split('*', 1)[0]
				
				# add to list if not empty
				if chip != '' and '(' in chip:
					chips.append(chip.split('(')[0])
					
		return chips
			
		
	# true if parts of chip are known, else false
	def hasSpecs(self, chip):
	
		# known
		if chip in self.specs:
		
			# has sub-parts
			if len(self.specs[chip]) > 0:
				for subChip in self.specs[chip]:
				
					# known sub-part
					if self.hasSpecs(subChip):
						return True
						
			# given 
			else:
				return True
				
		# unknown
		else:
			return False
					
	
	# recursively print chip tree
	def printChipTreeR(self, chip, layer, printEnable):
	
		# print chip, filtering by layer and redundancy
		if layer <= int(self.L) and printEnable:
			for i in range(0, layer):
				print ' ',
			print('-' + chip)
			
		# duplicate check (only print each chip and its components once)
		inDups = chip in self.dups
		self.dups.append(chip)
		
		# print sub-chips
		for subChip in self.specs[chip]:
			for i in range(0, self.specs[chip][subChip]):
				self.printChipTreeR(subChip, layer+1, True)
			
		
	# returns # nands + bits of a chip
	# -only gets called when hasSpecs is True
	def getCounts(self, chip):
	
		# counts known
		if chip in self.counts:
			return self.counts[chip]
			
		# counts unknown
		else:
		
			# sum nands + bits
			sumN = 0
			sumB = 0
			
			# get counts on sub-chips
			for subChip in self.specs[chip]:
				for i in range(0, self.specs[chip][subChip]):
					sum = self.getCounts(subChip)
					sumN += sum[0]
					sumB += sum[1]
			
			# counts found
			self.counts[chip] = [sumN, sumB]
			return [sumN, sumB]
		
	

if __name__ == '__main__':
	main()