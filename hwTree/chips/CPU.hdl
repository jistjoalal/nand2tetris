// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/05/CPU.hdl

/**
 * The Hack CPU (Central Processing unit), consisting of an ALU,
 * two registers named A and D, and a program counter named PC.
 * The CPU is designed to fetch and execute instructions written in 
 * the Hack machine language. In particular, functions as follows:
 * Executes the inputted instruction according to the Hack machine 
 * language specification. The D and A in the language specification
 * refer to CPU-resident registers, while M refers to the external
 * memory location addressed by A, i.e. to Memory[A]. The inM input 
 * holds the value of this location. If the current instruction needs 
 * to write a value to M, the value is placed in outM, the address 
 * of the target location is placed in the addressM output, and the 
 * writeM control bit is asserted. (When writeM==0, any value may 
 * appear in outM). The outM and writeM outputs are combinational: 
 * they are affected instantaneously by the execution of the current 
 * instruction. The addressM and pc outputs are clocked: although they 
 * are affected by the execution of the current instruction, they commit 
 * to their new values only in the next time step. If reset==1 then the 
 * CPU jumps to address 0 (i.e. pc is set to 0 in next time step) rather 
 * than to the address resulting from executing the current instruction. 
 */

CHIP CPU {

    IN  inM[16],         // M value input  (M = contents of RAM[A])
        instruction[16], // Instruction for execution
        reset;           // Signals whether to re-start the current
                         // program (reset==1) or continue executing
                         // the current program (reset==0).

    OUT outM[16],        // M value output
        writeM,          // Write to M? 
        addressM[15],    // Address in data memory (of M)
        pc[15];          // address of next instruction

    PARTS:
	//instruction type
    Mux16(a=instruction, b=aluOut, sel=instruction[15], out=m1);
	
	//a load if: A-type or A= set in C-type
	Not(in=instruction[15], out=typeA);
	And(a=instruction[5], b=instruction[15], out=typeC);
	Or(a=typeA, b=typeC, out=aload);
	
	//a register
	ARegister(in=m1, load=aload, out=areg);
	
	//addressing mode
	And(a=instruction[12], b=instruction[15], out=amode);
	Mux16(a=areg, b=inM, sel=amode, out=term2);
	
	//d register
	And(a=instruction[4], b=instruction[15], out=dload);
	DRegister(in=aluOut, load=dload, out=dreg);
	
	//ALU (x=D, y=A)
	ALU(x=dreg, y=term2, zx=instruction[11], nx=instruction[10], zy=instruction[9], ny=instruction[8], f=instruction[7], no=instruction[6], out=aluOut, zr=zr, ng=ng);
	
	//Copy ALU output to outM
	And16(a[0..15]=true, b=aluOut, out=outM);
	
	//3rd bit is writeM, in C type
	And(a=instruction[3], b=instruction[15], out=writeM);
	
	//Copy a reg to addressM
	Trim1(in=areg, out=addressM);
	
	//determine jump
	
	//j1 & ng
	And(a=instruction[2], b=ng, out=x);
	//j2 & zr
	And(a=instruction[1], b=zr, out=y);
	//j3 & !ng
	Not(in=ng, out=notNg);
	Not(in=zr, out=notZr);
	And(a=notNg, b=notZr, out=greater);
	And(a=greater, b=instruction[0], out=z);
	
	//3-way or
	Or(a=x, b=y, out=o1);
	Or(a=z, b=o1, out=o2);
	And(a=o2, b=instruction[15], out=jump);
	
	//program counter
	PC(in=areg, load=jump, inc=true, reset=reset, out=opc);
	Trim1(in=opc, out=pc);
}