// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/DMux8Way.hdl

/**
 * 8-way demultiplexor:
 * {a, b, c, d, e, f, g, h} = {in, 0, 0, 0, 0, 0, 0, 0} if sel == 000
 *                            {0, in, 0, 0, 0, 0, 0, 0} if sel == 001
 *                            etc.
 *                            {0, 0, 0, 0, 0, 0, 0, in} if sel == 111
 */

CHIP DMux8Way {
    IN in, sel[3];
    OUT a, b, c, d, e, f, g, h;

    PARTS:
    DMux4Way(in=in, sel=sel[0..1], a=wa, b=wb, c=wc, d=wd);
	Not(in=sel[2], out=nsel);
	And(a=nsel, b=wa, out=a);
	And(a=nsel, b=wb, out=b);
	And(a=nsel, b=wc, out=c);
	And(a=nsel, b=wd, out=d);
	And(a=sel[2], b=wa, out=e);
	And(a=sel[2], b=wb, out=f);
	And(a=sel[2], b=wc, out=g);
	And(a=sel[2], b=wd, out=h);
}