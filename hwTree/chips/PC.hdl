// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/a/PC.hdl

/**
 * A 16-bit counter with load and reset control bits.
 * if      (reset[t] == 1) out[t+1] = 0
 * else if (load[t] == 1)  out[t+1] = in[t]
 * else if (inc[t] == 1)   out[t+1] = out[t] + 1  (integer addition)
 * else                    out[t+1] = out[t]
 */

CHIP PC {
    IN in[16],load,inc,reset;
    OUT out[16];

    PARTS:
	
    Register(in=loop, load=true, out=reg);
	
	Inc16(in=reg, out=incd);
	And16(a[0..15]=false, b[0..15]=false, out=zero);
	Mux16(a=reg, b=incd, sel=inc, out=w1);
	Mux16(a=w1, b=in, sel=load, out=w2);
	Mux16(a=w2, b=zero, sel=reset, out=loop);
	And16(a=reg, b[0..15]=true, out=out);
}
