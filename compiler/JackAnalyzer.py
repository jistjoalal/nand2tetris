import sys
import Analyzer


# checks usage and runs the syntax analyzer
# -t = token mode (grammar mode default)
# -d = debug console output
# -path can be single file or directory
def main():

    # usage
    numArgs = len(sys.argv)
    if numArgs < 2:
        print('Usage: python JackAnalyzer.py [-t, -d] (prog.jack | dir)')
        exit(1)

    # store path
    path = sys.argv[numArgs-1]
    grammar = True
    debug = False

    # store flags
    if numArgs > 2:
        # flags = all args except first and last
        flags = [sys.argv[i] for i in range(1, numArgs-1)]

        # set switches
        grammar = '-t' not in flags
        debug = '-d' in flags

    # runs analyze on path, passing flags
    Analyzer.analyze(path, grammar, debug)

    exit(0)


if __name__ == '__main__':
    main()
