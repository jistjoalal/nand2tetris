import os
from string import ascii_letters
from string import digits


# Analyzes the syntax of one or more .jack files
#  (ch. 10)
# -generated files are saved to (path)-s.xml
# -use grammar=True to generate language construct XML output
# -use grammar=False to generate token XML output
# -use debug=True for console output + output write
# -use debug=False for output write only


def analyze(path, grammar, debug):

    # select function based on grammar flag
    function = grammarXML if grammar else tokenXML

    # runs function on file or dir
    runOnFileOrDir(path, '.jack', function, debug, True)


# Compiles 1+ .jack files into .vm files
#  (ch. 11)
# output: (path)-s.vm
# -debug = console output
# -xml = XML output (is also written to file)
# -use both False for compiling, either or both True for debugging
def compile(path, debug, xml):

    # runs compileFile on path
    runOnFileOrDir(path, '.jack', compileFile, debug, xml)


# runs the passed function on all files matching type in path (or single file)
def runOnFileOrDir(path, fileType, function, debug, xml):

    # directory of files
    if fileType not in path and os.path.exists(path):

        # trim ending /
        if path[len(path)-1] == '/':
            path = path[:-1]

        # for each file run function
        for f in os.listdir(path):
            if fileType in f:
                function(path + '/' + os.path.basename(f), debug, xml)

    # single file
    else:
        function(path, debug, xml)


# generate vm command output from language constructs
def compileFile(path, debug, xml):

    statusMessage('compiling: ' + path, debug)

    # output filename
    fileName = path.strip('.jack')+'.vm'

    # compile file
    t = Tokenizer(path)
    c = CompilationEngine(t.tokens, fileName, debug, xml, True)
    c.compile()

    statusMessage(' output saved to: ' + fileName, debug)


# generate xml output from language constructs
def grammarXML(path, debug, xml):

    statusMessage('analyzing: ' + path, debug)

    # output filename
    fileName = path.strip('.jack')+'-s.xml'

    # compile file
    t = Tokenizer(path)
    c = CompilationEngine(t.tokens, fileName, debug, xml, False)
    c.compile()

    statusMessage('output saved to: ' + fileName, debug)


# generate xml output from tokens
def tokenXML(path, debug, xml):

    statusMessage('tokenizing: ' + path, debug)

    # output filename
    fileName = path.strip('.jack')+'T-s.xml'

    # initialize tokenizer and output file
    t = Tokenizer(path)

    # print tokens
    printTokens(t.tokens, fileName, debug, xml)

    statusMessage('output saved to: ' + fileName, debug)


# print tokens to file/debug
def printTokens(tokens, fileName, debug, xml):

    # open output file
    output = open(fileName, 'w')

    # open tag
    if xml:
        writeToFile('<tokens>\n', output, debug)

    # for each token
    for tok in tokens:

        # type token before formatting
        type = tokenType(tok)

        # trim double quotes + fix special chars
        token = formatToken(tok)

        # print token
        writeToFile('<'+type+'> '+token+' </'+type+'>\n', output, debug)

    # close tag
    if xml:
        writeToFile('</tokens>\n', output, debug)

    # close file
    output.close()


# tracks symbol information needed in VM Translation
class SymbolTable:
    def __init__(self):
        self.classScope = []
        self.subroutineScope = []
        self.kindCounts = {'STATIC': 0, 'FIELD': 0, 'ARG': 0, 'VAR': 0}

    # clear the subroutine scope
    def startSubroutine(self):
        self.kindCounts['ARG'] = 0
        self.kindCounts['VAR'] = 0
        self.subroutineScope = []

    # add a new identifier to the correct scope
    def define(self, name, typ, kind):
        k = kind.upper()

        # undefined
        if k not in self.kindCounts:
            print('ERROR: could not define ' + name + ', kind= ' + k)

        # add to table
        else:
            n = self.kindCounts[k]
            if k in ['ARG', 'VAR']:
                self.subroutineScope.append(Symbol(name, typ, k, n))
            elif k in ['STATIC', 'FIELD']:
                self.classScope.append(Symbol(name, typ, k, n))
            self.kindCounts[k] += 1

    def varCount(self, kind):
        if kind not in self.kindCounts:
            print('ERROR: could not count ' + kind)
            return -1
        else:
            return self.kindCounts[kind]

    def inTable(self, name):
        for s in self.classScope:
            if s.name == name:
                return True
        for s in self.subroutineScope:
            if s.name == name:
                return True
        return False

    def kindOf(self, name):
        for s in self.classScope:
            if s.name == name:
                return s.kind
        for s in self.subroutineScope:
            if s.name == name:
                return s.kind
        return 'NONE'

    def typeOf(self, name):
        for s in self.classScope:
            if s.name == name:
                return s.type
        for s in self.subroutineScope:
            if s.name == name:
                return s.type
        return 'TYPE_ERROR'

    def indexOf(self, name):
        for s in self.classScope:
            if s.name == name:
                return s.n
        for s in self.subroutineScope:
            if s.name == name:
                return s.n
        return 'INDEX_ERROR'


# one row of symbol table
class Symbol:
    def __init__(self, name, type, kind, n):
        self.name = name
        self.type = type
        self.kind = kind
        self.n = n


# compiles token list into grammar
# writes grammar structure to output:
# -XML output if xml = True
# -VM command output if
class CompilationEngine:

    # open output file and initialize variables
    # tokens = list
    # path = string
    # debug, xml, vmOut = bool
    def __init__(self, tokens, path, debug, xml, vmOut):
        self.tokens = tokens
        if not debug:
            self.output = open(path, 'w')
        else:
            self.output = None
        self.debug = debug
        self.xml = xml
        self.vmOut = vmOut

        self.s = SymbolTable()
        self.className = ''        # for function and call commands
        self.i = 0                # token list index
        self.whileCount = 0        # for label commands
        self.ifCount = 0
        self.buffer = ''        # for buffering partial vm commands
        self.functionType = 'function'

    # run compile tree starting with compileClass, close file
    def compile(self):
        if self.tokens[self.i] == 'class':
            self.className = self.tokens[self.i+1]
            self.compileClass(0)
        else:
            print(path + ' must begin with a class definition.')

        if not self.debug:
            self.output.close()

    """ output """

    # -tags are non-terminal elements
    # -tokens are terminal elements
    # -printVM outputs VM commands

    # print a structural tag (non-terminal)
    def printTag(self, type, offset, open):

        # decrement offset if close tag
        offset = offset-1 if not open else offset

        # output indentation
        self.printIndentation(offset)

        # open or close tag?
        slash = '<' if open else '</'

        # fix special chars
        type = SPECIAL_CHARS[type] if type in SPECIAL_CHARS else type

        # output tag
        if self.xml:
            writeToFile(slash + type + '>\n', self.output, self.debug)

        # increment offset if open tag
        offset = offset+1 if open else offset

        # for incrementing/decrementing offset
        return offset

    # print one formatted token (terminal), increment token index
    def printNextToken(self, offset):
        # output indentation
        self.printIndentation(offset)

        # token, type = format(token, type)

        # type token before formatting
        token = self.tokens[self.i]
        type = tokenType(token)

        # get special details for identifiers
        details = self.identifierDetails(self.i)

        # strip double quotes
        token = formatToken(token)

        # output token
        outputString = '<'+type+details+'> '+token+' </'+type+'>'
        if self.xml:
            writeToFile(outputString + '\n', self.output, self.debug)

        # increment token index
        self.i += 1

    # print n tokens
    def printTokens(self, offset, numTokens):
        for i in range(numTokens):
            self.printNextToken(offset)

    # print an indent of size (' ' * 2 * offset)
    def printIndentation(self, offset):
        for i in range(offset):
            if self.xml:
                writeToFile('  ', self.output, self.debug)

    # print vm command, buffering partial commands
    def writeVM(self, line):
        if self.vmOut:
            if '\n' in line:
                writeToFile(self.buffer + line, self.output, self.debug)
                self.buffer = ''
            else:
                self.buffer += line

    """ helper methods """

    # returns identifier detail string for xml output
    # manages symbol table
    def identifierDetails(self, index):
        token = self.tokens[index]
        type = tokenType(token)
        details = ''

        if type == 'identifier':
            # new subroutine
            if self.tokens[index-2] in ['function', 'method', 'constructor']:
                self.s.startSubroutine()
                if self.tokens[index-2] == 'method':
                    self.s.define(token, 'implicit object arg', 'ARG')
                details += ' category=subroutine isDefined=False'

            # call subroutine
            elif self.tokens[index+1] == '(':
                details += ' category=subroutine isDefined=True'

            # new class
            elif self.tokens[index-1] == 'class':
                details += ' category=class isDefined=False'

            # existing class
            elif ((self.tokens[index+1] == '.'
                    or self.tokens[index-1] in ID0
                    or self.tokens[index-4] in ID1)
                    and not self.s.inTable(token)):
                details += ' category=class isDefined=True'

            # new table row
            elif not self.s.inTable(token):
                typ = ''
                kind = ''

                # move index back until symbol, ignoring commas
                indexOfSymbol = index - 1
                while (self.tokens[indexOfSymbol] == ',' or
                        tokenType(self.tokens[indexOfSymbol]) != 'symbol'):
                    indexOfSymbol -= 1

                # move index back until comma or open parenthesis
                indexOfComma = index - 1
                while (self.tokens[indexOfComma] != ','
                       and self.tokens[indexOfComma] != '('):
                    indexOfComma -= 1

                # argument ( preceding symbol is '(' )
                if self.tokens[indexOfSymbol] == '(':
                    kind = 'ARG'
                    typ = self.tokens[indexOfComma+1]

                # field, static, local
                else:
                    kind = self.tokens[indexOfSymbol+1]
                    typ = self.tokens[indexOfSymbol+2]

                # add to table
                self.s.define(token, typ, kind)

                # add xml details
                details += ' category=' + self.s.kindOf(token)
                details += ' isDefined=False'
                details += ' n=' + str(self.s.indexOf(token))
            # already in table
            elif self.s.inTable(token):
                details += ' category=' + self.s.kindOf(token)
                details += ' isDefined=True'
                details += ' n=' + str(self.s.indexOf(token))

        return details

    # calls function(offset) if/until condition is evaluated to false
    # condition is a string

    def noneOrOne(self, condition, function, offset):
        if eval(condition):
            function(offset)

    def noneOrMore(self, condition, function, offset):
        while eval(condition):
            function(offset)

    """ engine """

    # -conditions are used to evaluate non-regular patterns of elements
    # -offset keeps track of current depth in grammar tree

    def compileClass(self, offset):
        # open tag
        offset = self.printTag('class', offset, True)

        # class, className, {
        self.printTokens(offset, 3)

        # 0 or more class variable declarations
        condition = "self.tokens[self.i] == 'static' \
                     or self.tokens[self.i] == 'field'"
        self.noneOrMore(condition, self.compileClassVarDec, offset)

        # 0 or more subroutine declarations
        condition = "self.tokens[self.i] == 'constructor' \
                     or self.tokens[self.i] == 'method' \
                     or self.tokens[self.i] == 'function'"
        self.noneOrMore(condition, self.compileSubroutineDec, offset)

        # }
        self.printNextToken(offset)

        # close tag
        offset = self.printTag('class', offset, False)

    def compileClassVarDec(self, offset):
        # open tag
        offset = self.printTag('classVarDec', offset, True)

        # (static | field), type, varName
        self.printTokens(offset, 3)

        # 0 or more (, varName)
        condition = "self.tokens[self.i] != ';'"
        self.noneOrMore(condition, self.printNextToken, offset)

        # ;
        self.printNextToken(offset)

        # close tag
        offset = self.printTag('classVarDec', offset, False)

    def compileSubroutineDec(self, offset):

        self.ifCount = 0
        self.whileCount = 0

        # open tag
        offset = self.printTag('subroutineDec', offset, True)

        self.functionType = self.tokens[self.i]

        # (constructor | function | methood), (void | type), subroutineName, (
        self.printTokens(offset, 4)

        functionName = self.tokens[self.i-2]

        # parameter list
        self.compileParameterList(offset)

        # )
        self.printNextToken(offset)

        vmCommand = 'function ' + self.className + '.' + functionName
        self.writeVM(vmCommand)

        # subroutineBody
        self.compileSubroutineBody(offset)

        # close tag
        offset = self.printTag('subroutineDec', offset, False)

    def compileParameterList(self, offset):
        # open tag
        offset = self.printTag('parameterList', offset, True)

        # 0 or more , type varName
        condition = "self.tokens[self.i] != ')'"
        self.noneOrMore(condition, self.printNextToken, offset)

        # close tag
        offset = self.printTag('parameterList', offset, False)

    def compileSubroutineBody(self, offset):
        # open tag
        offset = self.printTag('subroutineBody', offset, True)

        # {
        self.printNextToken(offset)

        # 0 or more varDec
        condition = "self.tokens[self.i] == 'var'"
        self.noneOrMore(condition, self.compileVarDec, offset)

        nVars = self.s.varCount('VAR')
        self.writeVM(' ' + str(nVars) + '\n')

        if self.functionType == 'constructor':
            nFields = self.s.kindCounts['FIELD']
            self.writeVM('push constant ' + str(nFields) + '\n')
            self.writeVM('call Memory.alloc 1\n')
            self.writeVM('pop pointer 0\n')
        elif self.functionType == 'method':
            self.writeVM('push argument 0\n')
            self.writeVM('pop pointer 0\n')

        # statements
        self.compileStatements(offset)

        # }
        self.printNextToken(offset)

        # close tag
        offset = self.printTag('subroutineBody', offset, False)

    def compileVarDec(self, offset):
        # open tag
        offset = self.printTag('varDec', offset, True)

        # var, type, varName
        self.printTokens(offset, 3)

        # 0 or more , varName
        condition = "self.tokens[self.i] != ';'"
        self.noneOrMore(condition, self.printNextToken, offset)

        # ;
        self.printNextToken(offset)

        # close tag
        offset = self.printTag('varDec', offset, False)

    def compileStatements(self, offset):
        # open tag
        offset = self.printTag('statements', offset, True)

        # 0 or more statements
        condition = "self.tokens[self.i] != '}'"
        self.noneOrMore(condition, self.compileStatement, offset)

        # close tag
        offset = self.printTag('statements', offset, False)

    def compileStatement(self, offset):
        if self.tokens[self.i] == 'let':
            self.compileLet(offset)
        elif self.tokens[self.i] == 'if':
            self.compileIf(offset)
        elif self.tokens[self.i] == 'while':
            self.compileWhile(offset)
        elif self.tokens[self.i] == 'do':
            self.compileDo(offset)
        elif self.tokens[self.i] == 'return':
            self.compileReturn(offset)

    def compileLet(self, offset):
        # open tag
        offset = self.printTag('letStatement', offset, True)

        varName = self.tokens[self.i+1]
        segment = SEGMENTS[self.s.kindOf(varName)]
        index = self.s.indexOf(varName)
        isArray = False

        # let, varName
        self.printTokens(offset, 2)

        # 0 or 1 [ expression ]
        if self.tokens[self.i] == '[':
            self.compileBracketExpression(offset)
            isArray = True

        # =
        self.printNextToken(offset)

        # expression
        self.compileExpression(offset)

        # ;
        self.printNextToken(offset)

        if isArray:
            self.writeVM('pop temp 0\n')
            self.writeVM('pop pointer 1\n')
            self.writeVM('push temp 0\n')
            self.writeVM('pop that 0\n')
        else:
            self.writeVM('pop ' + segment + ' ' + str(index) + '\n')

        # close tag
        offset = self.printTag('letStatement', offset, False)

    def compileBracketExpression(self, offset):

        arrayName = self.tokens[self.i-1]

        # [
        self.printNextToken(offset)
        # expression
        self.compileExpression(offset)
        # ]
        self.printNextToken(offset)

        self.writeVM('push ' + SEGMENTS[self.s.kindOf(arrayName)] +
                     ' ' + str(self.s.indexOf(arrayName)) + '\n')
        self.writeVM('add\n')

    def compileIf(self, offset):
        # open tag
        offset = self.printTag('ifStatement', offset, True)

        # if, (
        self.printTokens(offset, 2)

        # expression
        self.compileExpression(offset)

        # ), {
        self.printTokens(offset, 2)

        ic = str(self.ifCount)
        self.ifCount += 1
        self.writeVM('if-goto IF_TRUE' + ic + '\n')
        self.writeVM('goto IF_FALSE' + ic + '\n')
        self.writeVM('label IF_TRUE' + ic + '\n')

        # statements
        self.compileStatements(offset)

        # }
        self.printNextToken(offset)

        # 0 or 1 else statement
        elseExists = False
        if self.tokens[self.i] == 'else':

            self.writeVM('goto IF_END' + ic + '\n')
            self.writeVM('label IF_FALSE' + ic + '\n')

            # else statement
            self.compileElseStatement(offset)
            elseExists = True

        if elseExists:
            self.writeVM('label IF_END' + ic + '\n')
        else:
            self.writeVM('label IF_FALSE' + ic + '\n')

        # close tag
        offset = self.printTag('ifStatement', offset, False)

    def compileElseStatement(self, offset):
        # else, {
        self.printTokens(offset, 2)

        # statements
        self.compileStatements(offset)

        # }
        self.printNextToken(offset)

    def compileWhile(self, offset):
        # open tag
        offset = self.printTag('whileStatement', offset, True)

        # while, (
        self.printTokens(offset, 2)

        wc = str(self.whileCount)
        self.whileCount += 1
        self.writeVM('label WHILE_EXP' + wc + '\n')

        # expression
        self.compileExpression(offset)

        # ), {
        self.printTokens(offset, 2)

        self.writeVM('not' + '\n')
        self.writeVM('if-goto WHILE_END' + wc + '\n')

        # statements
        self.compileStatements(offset)

        # }
        self.printNextToken(offset)

        self.writeVM('goto WHILE_EXP' + wc + '\n')
        self.writeVM('label WHILE_END' + wc + '\n')

        # close tag
        offset = self.printTag('whileStatement', offset, False)

    def compileDo(self, offset):
        # open tag
        offset = self.printTag('doStatement', offset, True)

        # do
        self.printNextToken(offset)

        # subroutineCall
        self.compileSubroutineCall(offset)

        # ;
        self.printNextToken(offset)

        self.writeVM('pop temp 0' + '\n')

        # close tag
        offset = self.printTag('doStatement', offset, False)

    def compileReturn(self, offset):
        # open tag
        offset = self.printTag('returnStatement', offset, True)

        # return
        self.printNextToken(offset)

        # 0 or 1 expression
        if self.tokens[self.i] != ';':
            self.compileExpression(offset)
        else:
            # if no expression, push 0 as return value
            self.writeVM('push constant 0' + '\n')

        self.writeVM('return' + '\n')

        # ;
        self.printNextToken(offset)

        # close tag
        offset = self.printTag('returnStatement', offset, False)

    def compileExpression(self, offset):
        # open tag
        offset = self.printTag('expression', offset, True)

        # term
        self.compileTerm(offset)

        # 0 or more (op term)
        condition = "self.tokens[self.i] in OPS"
        self.noneOrMore(condition, self.compileOpTerm, offset)

        # close tag
        offset = self.printTag('expression', offset, False)

    def compileOpTerm(self, offset):

        op = self.tokens[self.i]

        # op
        self.printNextToken(offset)
        # term
        self.compileTerm(offset)

        self.writeVM(OP_TRANSLATE[op] + '\n')

    def compileSubroutineCall(self, offset):

        # class.function(e) or function(e)
        next = self.tokens[self.i]
        twoAhead = self.tokens[self.i+1]
        objectPass = 0
        if twoAhead == '.':
            tokensToPrint = 4
            if self.s.inTable(next):
                className = self.s.typeOf(self.tokens[self.i])
                objectPass = 1
                self.writeVM('push ' + SEGMENTS[self.s.kindOf(next)] + ' '
                             + str(self.s.indexOf(next)) + '\n')
            else:
                className = next
            functionName = self.tokens[self.i+2]
        else:
            tokensToPrint = 2
            className = self.className
            functionName = self.tokens[self.i]
            objectPass = 1
            self.writeVM('push pointer 0\n')

        self.printTokens(offset, tokensToPrint)

        openIndex = self.i

        # expressionList
        self.compileExpressionList(offset)

        # nArgs = # of vars between ( )
        argRange = self.tokens[openIndex: self.i]
        if len(argRange) >= 1:
            nArgs = argRange.count(',') + 1
        else:
            nArgs = 0
        nArgs += objectPass

        self.writeVM('call ' + className + '.' + functionName + ' '
                     + str(nArgs) + '\n')

        # )
        self.printNextToken(offset)

    def compileExpressionList(self, offset):
        # open tag
        offset = self.printTag('expressionList', offset, True)

        # 0 or 1 expressionLists
        condition = "self.tokens[self.i] != ')'"
        self.noneOrOne(condition, self.compileFirstExpressionInList, offset)

        # close tag
        offset = self.printTag('expressionList', offset, False)

    def compileFirstExpressionInList(self, offset):
        # expression
        self.compileExpression(offset)

        # 0 or more (, expression)
        condition = "self.tokens[self.i] != ')'"
        self.noneOrMore(condition, self.compileNextExpressionInList, offset)

    def compileNextExpressionInList(self, offset):
        # ,
        self.printNextToken(offset)

        # expression
        self.compileExpression(offset)

    def compileTerm(self, offset):
        # open tag
        offset = self.printTag('term', offset, True)

        # local variables
        next = self.tokens[self.i]
        twoAhead = self.tokens[self.i+1]

        # varName[ expression ]
        if twoAhead == '[':
            # varName, [
            self.printTokens(offset, 2)
            # expression
            self.compileExpression(offset)
            # ]
            self.printNextToken(offset)

            self.writeVM('push ' + SEGMENTS[self.s.kindOf(next)] + ' '
                         + str(self.s.indexOf(next)) + '\n')
            self.writeVM('add\n')
            self.writeVM('pop pointer 1\n')
            self.writeVM('push that 0\n')

        # subroutineCall
        elif validIdentifier(next) and (twoAhead == '(' or twoAhead == '.'):
            self.compileSubroutineCall(offset)

        # ( expression )
        elif next == '(':
            # (
            self.printNextToken(offset)
            # expression
            self.compileExpression(offset)
            # )
            self.printNextToken(offset)

        # unaryOp term
        elif next in UNARY_OPS:
            # unaryOp
            self.printNextToken(offset)
            # term
            self.compileTerm(offset)

            self.writeVM(UNARY_OP_TRANSLATE[next] + '\n')

        # default: constant
        else:
            self.printNextToken(offset)

            if tokenType(next) == 'integerConstant':
                self.writeVM('push constant ' + next + '\n')

            elif tokenType(next) == 'identifier':
                self.writeVM('push ' + SEGMENTS[self.s.kindOf(next)] + ' '
                             + str(self.s.indexOf(next)) + '\n')

            elif next in ['false', 'true']:
                self.writeVM('push constant 0' + '\n')
                if next == 'true':
                    self.writeVM('not' + '\n')

            elif tokenType(next) == 'stringConstant':
                self.writeVM('push constant ' + str(len(next)-2) + '\n')
                self.writeVM('call String.new 1' + '\n')
                s = next.strip('"')
                for c in s:
                    self.writeVM('push constant ' + str(ord(c)) + '\n')
                    self.writeVM('call String.appendChar 2\n')

            elif next == 'this':
                self.writeVM('push pointer 0\n')

            elif next == 'null':
                self.writeVM('push constant 0\n')

        # close tag
        offset = self.printTag('term', offset, False)


# tokenizes a single .jack file
class Tokenizer:
    # open/close input file and builds list of tokens
    def __init__(self, path):
        # open input file
        if '.jack' in path:
            self.file = open(path)

        # build list of all chars that make up tokens
        self.chars = []
        self.relevantChars()

        # close input file
        self.file.close()

        # build list of tokens from chars
        self.tokens = []
        self.tokensFromChars()

    # builds self.tokens from self.chars by stepping thru chars
    def tokensFromChars(self):
        token = ''
        for i in range(len(self.chars)):
            # only include spaces in strings
            if self.chars[i] not in [' ', '\t'] or '"' in token:
                token += self.chars[i]

            # is token complete?
            if (
              # case0: token is a keyword
              # possible issue if keyword is part of identifier or string
              token in KEYWORDS and self.chars[i+1] == ' '

              # case1: token is a symbol
              or token in SYMBOLS

              # case2: char[i+1] is a symbol (token = int or identifier)
              or (self.chars[i+1] in SYMBOLS and '"' not in token)

              # case3: char[i] is space (token = int or identifier)
              # -followed by non-string space
              or (self.chars[i] == ' ' and '"' not in token)

              # string case
              or (self.chars[i] == '"' and self.chars[i+1] in SYMBOLS)
            ):
                # append non-empty tokens and reset for next
                if token != '':
                    self.tokens.append(token)
                    token = ''

    # builds self.chars from self.file
    def relevantChars(self):

        ignore = False
        for line in self.file:
            # remove enclosing whitespace
            trim = line.strip()

            # remove 1-line comments
            trim = trim.split('//', 1)[0]

            # remove multi line comments
            if '/*' in trim:
                if '*/' in trim:
                    trim = trim.split('*/', 1)[1]
                    ignore = False
                else:
                    trim = trim.split('/*', 1)[0]
                    ignore = True
            if '*/' in trim:
                trim = trim.split('*/', 1)[1]
                ignore = False
            # append relevant characters, ignore multi-line comment inner lines
            if trim != '' and not ignore:
                for c in trim:
                    self.chars.append(c)


# prints a status message if !debug
def statusMessage(text, debug):
    if not debug:
        print(text)


# write text to file | console if debug=True
def writeToFile(text, file, debug):
    if debug:
        print(text, end='')
    else:
        file.write(text)


# returns true if s is a valid identifer (a-z, 0-9, _)
def validIdentifier(s):
    valid_chars = set(list(ascii_letters) + list(digits) + ['_'])
    for c in s:
        if c not in valid_chars:
            return False
    return True


# formats token for XML output
def formatToken(token):
    # trim double quotes from output
    token = token.strip('"')

    # fix special chars
    token = SPECIAL_CHARS[token] if token in SPECIAL_CHARS else token

    return token


# returns type of given token
def tokenType(token):
    if token in KEYWORDS:
        return 'keyword'
    elif token in SYMBOLS:
        return 'symbol'
    elif token.isdigit():
        return 'integerConstant'
    elif '"' in token:
        return 'stringConstant'
    elif validIdentifier(token):
        return 'identifier'
    else:
        print('UNKNOWN_TOKEN: ' + token)
        return 'UNKNOWN_TOKEN'


# terminals(for parsing) and special characters(for XML output)
KEYWORDS = ['class', 'constructor', 'function', 'method', 'field', 'static',
            'var', 'int', 'char', 'boolean', 'void', 'true', 'false', 'null',
            'this', 'let', 'do', 'if', 'else', 'while', 'return'
            ]
SYMBOLS = ['{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/',
           '&', '|', '<', '>', '=', '~'
           ]
OPS = ['+', '-', '*', '/', '&', '|', '<', '>', '=']
UNARY_OPS = ['-', '~']
OP_TRANSLATE = {'+': 'add', '-': 'sub', '*': 'call Math.multiply 2',
                '/': 'call Math.divide 2',
                '&': 'and', '|': 'or', '<': 'lt', '>': 'gt', '=': 'eq'
                }
UNARY_OP_TRANSLATE = {'-': 'neg', '~': 'not'}
SEGMENTS = {'VAR': 'local', 'ARG': 'argument', 'FIELD': 'this',
            'STATIC': 'static'}
SPECIAL_CHARS = {'<': '&lt;', '>': '&gt;', '"': '&quot;', '&': '&amp;'}
ID0 = ['var', 'field', 'static', 'function', 'method', 'constructor']
ID1 = ['function', 'method', 'constructor', '(']
