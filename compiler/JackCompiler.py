import sys
import Analyzer


# checks usage and runs the compiler
# -path can be single file or directory
# -d = debug console output
# -x = xml output
def main():

    # usage
    numArgs = len(sys.argv)
    if numArgs < 2:
        print('Usage: python JackCompiler.py [-d -x] (prog.jack | dir)')
        exit(1)

    # store path
    path = sys.argv[numArgs-1]
    debug = False
    xml = False

    # store flags
    if numArgs > 2:
        # flags = all args except first and last
        flags = [sys.argv[i] for i in range(1, numArgs-1)]

        # set switches
        debug = '-d' in flags
        xml = '-x' in flags

    # runs compile on path, passing flags
    Analyzer.compile(path, debug, xml)

    exit(0)


if __name__ == '__main__':
    main()
