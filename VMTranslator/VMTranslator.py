import os
import sys


# Translates .vm files to .asm files
# Single file mode or directory mode
def main():

    # usage
    numArgs = len(sys.argv)
    if numArgs < 2:
        print('Usage: python VMTranslator.py [-d, -c] (prog.vm | dir)')
        exit(1)

    # store path
    path = sys.argv[numArgs-1]
    debug = False
    commentRelay = False

    # store flags
    if numArgs > 2:

        # flags = all args except first and last
        flags = [sys.argv[i] for i in range(1, numArgs-1)]

        # set switches
        debug = '-d' in flags
        commentRelay = '-c' in flags

    # translate input to .asm
    v = VMTranslator(path, debug, commentRelay)
    v.translate()

    exit(0)


class VMTranslator:
    def __init__(self, path, debug, commentRelay):
        self.path = path
        self.debug = debug
        self.commentRelay = commentRelay
        self.files = []
        self.p = None
        self.s = None

    # translates directory or single .vm file into .asm
    def translate(self):

        # directory
        if '.vm' not in self.path and os.path.exists(self.path):
            self.initDir()

        # single file
        else:
            self.initFile()

        # initialize codewriter and translate
        self.c.writeInit()
        self.translateFiles()
        self.c.close()

    # initialize codewriter for directory mode
    def initDir(self):

        # trim ending /
        if self.path[len(self.path)-1] == '/':
            self.path = self.path[:-1]

        # for each file run function
        for f in os.listdir(self.path):
            if '.vm' in f:

                # full path
                self.files.append(self.path + '/' + f)

        # compile all tranlated asm code into [directory name].asm
        self.c = CodeWriter(self.path + '/'
                            + os.path.basename(self.path) + '.asm', self.debug)

    # intialize codewriter for single file mode
    def initFile(self):
        self.files = [self.path]
        self.c = CodeWriter(self.path.split('.vm')[0] + '.asm', self.debug)

    # translate each file
    def translateFiles(self):
        for filename in self.files:

            # translate commands for each file
            self.translateCommands(filename)

    # translate commands for one file, updating parser and codewriter
    def translateCommands(self, filename):

        # set codewriter filename + initialize parser
        self.c.setFilename(os.path.basename(filename))
        self.p = Parser(filename)

        # translate each commanad
        while (self.p.hasMoreCommands()):
            self.p.advance()

            # include vm comments
            if self.commentRelay:
                self.c.writeVMComment(self.p.currentCommand)

            # get current command type
            cType = self.p.commandType()

            if (cType == 'PUSH'
                    or cType == 'POP'):
                self.c.writePushPop(cType, self.p.arg1(), self.p.arg2())

            elif cType == 'ARITHMETIC':
                self.c.writeArithmetic(self.p.currentCommand.strip())

            elif cType == 'LABEL':
                self.c.writeLabel(self.p.arg1())

            elif cType == 'GOTO':
                self.c.writeGoto(self.p.arg1())

            elif cType == 'IF':
                self.c.writeIf(self.p.arg1())

            elif cType == 'CALL':
                self.c.writeCall(self.p.arg1(), self.p.arg2())

            elif cType == 'RETURN':
                self.c.writeReturn()

            elif cType == 'FUNCTION':
                self.c.writeFunction(self.p.arg1(), self.p.arg2())


# writes translated asm code to output file
class CodeWriter:
    def __init__(self, outputFile, debug):
        self.f = open(outputFile, 'w')
        self.debug = debug

        # title
        self.debugWrite('// ' + outputFile + '\n\n')

        # temp vars for labels
        self.tfCount = 0
        self.returnCount = 0
        self.function = ''

    def debugWrite(self, text):
        if self.debug:
            print(text, end='')
        else:
            self.f.write(text)

    # marks filename
    def setFilename(self, filename):
        self.filename = os.path.basename(filename).strip('.vm')
        self.debugWrite('\n// ' + filename + '\n\n')

    # VM code comment
    def writeVMComment(self, comment):
        self.debugWrite('\n// ' + comment + '\n')

    def writeArithmetic(self, command):
        # 1 arg operation
        if command.upper() in ONE_ARG_OP:
            self.debugWrite(
                # grab a
                POPA
                # arithmetic + write
                + ONE_ARG_OP[command.upper()]
            )
        # 2 arg operation
        elif command.upper() in TWO_ARG_OP:
            self.debugWrite(
                # grab a and b
                POPA + POPB
                # arithmetic + write
                + TWO_ARG_OP[command.upper()]
                # sp--
                + SPD
            )
        # TRUE / FALSE operation
        elif command.upper() in TF_OP:
            self.debugWrite(
                # grab a and b
                POPA + POPB
                # jump condition
                + TF_PREFIX + str(self.tfCount) + TF_OP[command.upper()]
                # write options
                + str(self.tfCount).join(FALSE)
                + str(self.tfCount).join(TRUE)
                # sp--
                + SPD
            )
            self.tfCount += 1
        # unknown
        else:
            print('Unknown arithmetic command: ' + command)

    def writePushPop(self, command, segment, index):
        s = ''
        if command == 'PUSH':
            if segment == 'constant':
                s += '@' + str(index) + '\nD=A \n@SP \nA=M \nM=D \n'
            elif segment == 'local':
                s += '@LCL \nD=M \n@' + str(index) + PUSH
            elif segment == 'argument':
                s += '@ARG \nD=M \n@' + str(index) + PUSH
            elif segment == 'this':
                s += '@THIS \nD=M \n@' + str(index) + PUSH
            elif segment == 'that':
                s += '@THAT \nD=M \n@' + str(index) + PUSH
            elif segment == 'pointer':
                s += '@3 \nD=A \n@' + str(index) + PUSH
            elif segment == 'temp':
                s += '@5 \nD=A \n@' + str(index) + PUSH
            elif segment == 'static':
                s += ('@'+self.filename+'.'+str(index)
                      + '\nD=M \n@SP \nA=M \nM=D \n')
            s += SPI

        elif command == 'POP':

            if segment == 'static':
                s += (POPA + 'D=M \n'+'@'+self.filename+'.'+str(index)
                      + '\nM=D \n' + SPD)
            else:
                if segment == 'local':
                    s += '@LCL \nD=M \n@'
                elif segment == 'argument':
                    s += '@ARG \nD=M \n@'
                elif segment == 'this':
                    s += '@THIS \nD=M \n@'
                elif segment == 'that':
                    s += '@THAT \nD=M \n@'
                elif segment == 'pointer':
                    s += '@3 \nD=A \n@'
                elif segment == 'temp':
                    s += '@5 \nD=A \n@'
                s += (str(index) + '\nD=D+A \n@R12'
                      + '\nM=D \n' + POPA + 'D=M \n@R12'
                      + '\nA=M \nM=D \n' + SPD)

        self.debugWrite(s)

    def writeInit(self):
        self.debugWrite('// Initialization \n\n')
        self.debugWrite('@256 \nD=A \n@SP \nM=D \n')  # SP=256
        self.writeCall('Sys.init', 0)  # call Sys.init

    def writeLabel(self, label):
        if self.function != '':
            self.debugWrite('(' + self.function + '$' + label + ')\n')
        else:
            self.debugWrite('(' + label + ')\n')  # assembly label

    def writeGoto(self, label):
        if self.function != '':
            self.debugWrite('@' + self.function + '$' + label + '\n0;JMP \n')
        else:
            self.debugWrite('@' + label + '\n0;JMP \n')  # jump to label

    def writeIf(self, label):
        self.debugWrite('@SP \nAM=M-1 \nD=M \n')  # pop into D
        if self.function != '':
            self.debugWrite('@' + self.function + '$' + label + '\nD;JNE \n')
        else:
            self.debugWrite('@' + label + '\nD;JNE \n')  # jump to label if !0

    def writeCall(self, functionName, numArgs):
        # push return address
        self.debugWrite('@ret_' + str(self.returnCount)
                        + '\nD=A \n@SP \nA=M \nM=D \n' + SPI)
        # push frame
        for seg in ['LCL', 'ARG', 'THIS', 'THAT']:
            self.debugWrite('@' + seg + '\nD=M \n@SP \nA=M \nM=D \n' + SPI)
        # resposition ARG to SP-(numArgs+5)
        self.debugWrite('@SP \nD=M \n@5 \nD=D-A \n@' + str(numArgs)
                        + '\nD=D-A \n@ARG \nM=D \n')
        # resposition LCL to SP
        self.debugWrite('@SP \nD=M \n@LCL \nM=D \n')
        # goto function
        self.debugWrite('@' + functionName + '\n0;JMP \n')
        # return address label
        self.debugWrite('(ret_' + str(self.returnCount) + ')\n')
        # increment returnCount for indexing labels
        self.returnCount += 1

    def writeReturn(self):
        # FRAME = LCL
        self.debugWrite('@LCL \nD=M \n@R13 \nM=D \n')
        # store return address (*(FRAME-5)) in temp var
        self.debugWrite('@5 \nA=D-A \nD=M \n@R14 \nM=D \n')
        # reposition return value
        self.debugWrite(POPA + 'D=M \n@ARG \nA=M \nM=D \n')
        # restore SP
        self.debugWrite('@ARG \nD=M+1 \n@SP \nM=D \n')
        # restore state
        for seg in ['THAT', 'THIS', 'ARG', 'LCL']:
            self.debugWrite('@R13 \nD=M-1 \nAM=D \nD=M \n@' + seg + '\nM=D \n')
        # goto return address
        self.debugWrite('@R14 \nA=M \n0;JMP \n')

    def writeFunction(self, functionName, numLocals):
        self.function = functionName
        self.debugWrite('(' + functionName + ')\n')
        for i in range(numLocals):
            self.writePushPop('PUSH', 'constant', 0)

    def close(self):
        self.f.close()


# provides access to vm commands and their components of given .vm file
class Parser:
    def __init__(self, path):
        self.commands = []
        self.index = 0
        self.currentCommand = ''
        # grab all commands from file, store in list
        if '.vm' in path:
            self.commands = commandsFromFile(path)

    # for navigating translation
    def hasMoreCommands(self):
        return (len(self.commands) > self.index)

    # advance through input commands
    def advance(self):
        self.currentCommand = self.commands[self.index]
        self.index += 1

    # returns string identifying type of currentCommand
    def commandType(self):
        types = {'ARITHMETIC': ['add', 'sub', 'neg', 'eq', 'gt', 'lt', 'and',
                                'or', 'not'],
                 'PUSH': ['push'],
                 'POP': ['pop'],
                 'LABEL': ['label'],
                 'IF': ['if-goto'],
                 'GOTO': ['goto'],
                 'FUNCTION': ['function'],
                 'RETURN': ['return'],
                 'CALL': ['call']
                 }
        i = 0
        for t in types:
            for keyword in types[t]:
                if keyword == self.currentCommand.split(' ')[0]:
                    return list(types.keys())[i]
            i += 1
        return 'UNKNOWN'

    # returns vm command's 2nd arg (1st if only 1)
    def arg1(self):
        if len(self.currentCommand.split()) > 1:
            return self.currentCommand.split()[1]
        else:
            return self.currentCommand.split()[0]

    # return vm command's 3rd arg
    def arg2(self):
        return int(self.currentCommand.split()[2])


# returns list of commands from .vm file (filters irrelevant chars)
def commandsFromFile(file):
    commands = []
    with open(file) as f:
        for line in f:
            # remove white space
            command = line.strip()
            # remove comments
            command = command.split('//', 1)[0]
            # add to list if not empty
            if command != '':
                commands.append(command)
    return commands


# VM Sub-Command Constants
# used to build a VM command out of assembly commands

PUSH = '\nA=D+A \nD=M \n@SP \nA=M \nM=D \n'

# basic stack movements
POPA = '@SP \nA=M-1 \n'
POPB = 'D=M \nA=A-1 \n'

# sp -= 1
SPD = '@SP \nM=M-1 \n'
# sp += 1
SPI = '@SP \nM=M+1 \n'

# 1 arg ops: (operation on D reg)
ONE_ARG_OP = {
    'NEG': 'M=-M \n',
    'NOT': 'M=!M \n'
}

# 2 arg ops (operation on D and M reg)
TWO_ARG_OP = {
    'ADD': 'M=D+M \n',
    'SUB': 'M=M-D \n',
    'AND': 'M=D&M \n',
    'OR': 'M=D|M \n'
}

# T/F ops (operation on D and M reg, jump)
# uses tfCount to index
TF_PREFIX = 'D=M-D \n@TRUE'
TF_OP = {
    'EQ': '\nD;JEQ \n',
    'GT': '\nD;JGT \n',
    'LT': '\nD;JLT \n'
}

# TRUE FALSE blocks
# will be joined with tfCount to ensure unique jump labels
FALSE = [
    '(FALSE',
    ') \n@SP \nA=M-1 \nA=A-1 \nM=0 \n@END',
    '\n0;JMP \n'
]
TRUE = [
    '(TRUE',
    ') \n@SP \nA=M-1 \nA=A-1 \nM=-1 \n(END',
    ') \n'
]


if __name__ == '__main__':
    main()
