import sys


# Assembles .asm files into .hack files
#   -.asm files are text files containing assembly commands in the "Hack"
#     machine language
#   -.hack files are text files containing 16-bit binary machine commands
#     for the "Hack" hardware architecture
#   -"Hack" is a computing platform designed for the online course
#     nand2tetris.org
# TODO
# -
def main():

    # usage
    numArgs = len(sys.argv)
    if numArgs < 2:
        print('Usage: python Assembler.py [prog.asm]')
        exit(1)

    # store filename
    filename = sys.argv[numArgs-1]
    debug = False
    commentRelay = False

    # store flags
    if numArgs > 2:

        # flags = all args except first and last
        flags = [sys.argv[i] for i in range(1, numArgs-1)]

        # set switches
        debug = '-d' in flags
        commentRelay = '-c' in flags

    # run assembler on filename, passing flags
    a = Assembler(filename, debug, commentRelay)
    a.assemble()

    exit(0)


# handles the assembly of .asm files into .hack binary files
class Assembler:
    def __init__(self, filename, debug, commentRelay):
        self.filename = filename
        self.debug = debug
        self.p = Parser(filename, commentRelay)
        self.s = SymbolTable()
        self.vars = 0
        self.output = []

    # fully assembles .asm file into .hack file
    def assemble(self):
        self.buildSymbolTable()
        self.translate()
        self.writeOutput()

    # build symbol table
    def buildSymbolTable(self):

        # keep track of ROM address while advancing thru asm commands
        address = 0
        while self.p.hasMoreCommands():
            self.p.advance()

            # add labels to symbol table
            if self.p.commandType() == 'L':
                if not self.s.contains(self.p.symbol()):
                    self.s.addEntry(self.p.symbol(), address)

            # advance ROM address for each non-L instruction
            else:
                address += 1

    # translate ASM commands into HACK binary
    def translate(self):

        # reset parser to line 0
        self.p.index = 0

        # translation
        while self.p.hasMoreCommands():
            self.p.advance()

            # ignore labels
            if self.p.commandType() != 'L':
                self.translateInstruction()

    # translate a single instruction
    def translateInstruction(self):
        # comment relay
        if self.p.commandType() == '//':
            self.output.append('\n' + self.p.currentCommand)

        # A-Instruction
        elif self.p.commandType() == 'A':
            self.translateAInstruction()

        # C-Instruction
        elif self.p.commandType() == 'C':
            self.appendOutput('111' + COMP[self.p.comp()]
                              + DEST[self.p.dest()]
                              + JUMP[self.p.jump()])

        # Unknown command
        else:
            print('Unknown command: ' + self.p.currentCommand)
            exit(1)

    # translate a single A-type instruction
    def translateAInstruction(self):

        # numeric
        if toBinary(self.p.symbol()) != 'NaN':
            self.appendOutput(toBinary(self.p.symbol()))

        # non-numeric (symbol)
        else:

            # in symbol table
            if self.s.contains(self.p.symbol()):
                self.appendOutput(toBinary(
                    self.s.getAddress(self.p.symbol())))

            # not in table (new variable)
            else:
                self.s.addEntry(self.p.symbol(), 16 + self.vars)
                self.vars += 1
                self.appendOutput(toBinary(
                    self.s.getAddress(self.p.symbol())))

    # appends line to output list
    def appendOutput(self, line):

        # debug assembly comments
        if self.debug:
            line += '\t// ' + self.p.currentCommand

        # output
        self.output.append(line)

    # write output list to .hack file
    def writeOutput(self):

        # write each output line to file
        with open(self.filename[:-4] + '-s.hack', 'w') as f:
            for line in self.output:

                # out = terminal if debug else file
                if self.debug:
                    print(line)
                else:
                    f.write(line + '\n')


# builds table of symbols and their corresponding ROM addresses
class SymbolTable:
    def __init__(self):
        self.table = {'SP': 0,
                      'LCL': 1,
                      'ARG': 2,
                      'THIS': 3,
                      'THAT': 4,
                      'R0': 0, 'R1': 1, 'R2': 2, 'R3': 3, 'R4': 4, 'R5': 5,
                      'R6': 6, 'R7': 7, 'R8': 8, 'R9': 9, 'R10': 10,
                      'R11': 11, 'R12': 12, 'R13': 13, 'R14': 14, 'R15': 15,
                      'SCREEN': 16384,
                      'KBD': 24576
                      }

    def addEntry(self, symbol, address):
        self.table[symbol] = address

    def contains(self, symbol):
        return symbol in self.table

    def getAddress(self, symbol):
        return self.table[symbol]


# provides access to mnemonic commands and their components of given .asm file
class Parser:
    def __init__(self, filename, commentRelay):
        self.commands = []
        self.index = 0
        self.currentCommand = ''
        with open(filename) as f:
            for line in f:
                # remove extra white space
                command = line.strip()
                # remove comments
                if not commentRelay:
                    command = command.split('//', 1)[0].strip()
                # add to list if not empty
                if command != '':
                    self.commands.append(command)

    def hasMoreCommands(self):
        return (len(self.commands) > self.index)

    def advance(self):
        self.currentCommand = self.commands[self.index]
        self.index += 1

    def commandType(self):
        if '//' in self.currentCommand:
            return '//'
        elif '@' in self.currentCommand:
            return 'A'
        elif '=' in self.currentCommand or ';' in self.currentCommand:
            return 'C'
        elif '(' in self.currentCommand and ')' in self.currentCommand:
            return 'L'
        else:
            return '0'  # unknown command type

    # returns symbol component of A or L  command
    def symbol(self):
        if '@' in self.currentCommand:
            return self.currentCommand.strip('@').strip()
        elif '(' in self.currentCommand:
            return self.currentCommand[1:][:-1].strip()
        else:
            return 'UNKNOWN SYMBOL'

    # returns destination component of C command
    def dest(self):
        if '=' in self.currentCommand:
            return self.currentCommand.split('=')[0].strip()
        else:
            return ''   # null destination

    # returns computation component of C command
    def comp(self):
        c = self.currentCommand
        if '=' in c:
            return c.split('=')[1].strip()
        elif ';' in c:
            return c.split(';')[0].strip()
        else:
            return ''   # null comp (doesn't affect memory/registers)

    # returns jump component of C command
    def jump(self):
        if ';' in self.currentCommand:
            return self.currentCommand.split(';')[1].strip()
        else:
            return ''   # null jump


# returns 2s comp of dec as 16-bit binary string
def toBinary(dec):
    try:
        s = bin(int(dec) & int('1'*16, 2))[2:]
        return '{0:0>16}'.format(s)
    except ValueError:
        return 'NaN'


# decodes destination field
DEST = {
    '':    '000',
    'M':   '001',
    'D':   '010',
    'MD':  '011',
    'A':   '100',
    'AM':  '101',
    'AD':  '110',
    'AMD': '111'
}

# decodes computation field
COMP = {
    '':    'error',
    '0':   '0101010',
    '1':   '0111111',
    '-1':  '0111010',
    'D':   '0001100',
    'A':   '0110000',
    'M':   '1110000',
    '!D':  '0001101',
    '!A':  '0110001',
    '!M':  '1110001',
    '-D':  '0001111',
    '-A':  '0110011',
    '-M':  '1110011',
    'D+1': '0011111',
    'A+1': '0110111',
    'M+1': '1110111',
    'D-1': '0001110',
    'A-1': '0110010',
    'M-1': '1110010',
    'D+A': '0000010',
    'D+M': '1000010',
    'D-A': '0010011',
    'D-M': '1010011',
    'A-D': '0000111',
    'M-D': '1000111',
    'D&A': '0000000',
    'D&M': '1000000',
    'D|A': '0010101',
    'D|M': '1010101'
}

# decodes jump condition field
JUMP = {
    '':    '000',
    'JGT': '001',
    'JEQ': '010',
    'JGE': '011',
    'JLT': '100',
    'JNE': '101',
    'JLE': '110',
    'JMP': '111'
}


if __name__ == '__main__':
    main()
