String[] lines;
int R = 1;

void setup() {
  size(802,802);
  background(0);
  fill(200);
  noStroke();
  lines = loadStrings("My OS-s.hack");
  int index = 0;
  for (int i = 0; i < lines.length; i++) {
    for (int j = 0; j < lines[i].length(); j++) {
      if (lines[i].charAt(j) == '1') {
        rect(index % width, index / height, R, R);
      }
      index += R;
    }
  }
}

void draw() {
}