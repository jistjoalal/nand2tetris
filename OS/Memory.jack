// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/12/Memory.jack

/**
 * This library provides two services: direct access to the computer's main
 * memory (RAM), and allocation and recycling of memory blocks. The Hack RAM
 * consists of 32,768 words, each holding a 16-bit binary number.
 */ 
class Memory {

    // Hack system specifications
    static Array memory;
    static int HEAP, MEM_END, MAX_SIZE;

    // freeList - linked list of free memory blocks
    // 0    FL_SIZE     size of block
    // 1    FL_NEXT     ptr to next free block
    // 2    [Block]     [free]
    // ...  
    static Array freeList;
    static int FL_SIZE, FL_NEXT;

    // memory block allocation
    // -1   ALLOC_SIZE  size of block
    // 0    [Block]     [data]
    // ...  
    static int ALLOC_SIZE;
	

    /** Initializes the class. */
    function void init() {

        // hack memory specs
        let memory = 0;
        let HEAP = 2048;
        let MEM_END = 16384;
        let MAX_SIZE = MEM_END - HEAP;

        // freeList starts at bottom of heap
        let freeList = HEAP;
        let FL_SIZE = 0;
        let FL_NEXT = 1;

        // memory block allocation
        let ALLOC_SIZE = -1;

        // initialize freeList
        let freeList[FL_SIZE] = MEM_END - HEAP;
        let freeList[FL_NEXT] = null;

		return;
    }

    /** Returns the RAM value at the given address. */
    function int peek(int address) {
		return memory[address];
    }

    /** Sets the RAM value at the given address to the given value. */
    function void poke(int address, int value) {
		let memory[address] = value;
		return;
    }

    // props to the pioneer!
    // https://github.com/havivha/Nand2Tetris/blob/master/12/Memory.jack

    /** Finds an available RAM block of the given size and returns
     *  a reference to its base address. */
    function int alloc(int size) {
		var Array prevBlock, foundBlock;

        // allocated memory size must be positive
        if (size < 0) {
            do Sys.error(5);
        }

        // attempt to find fit
        let prevBlock = Memory.bestFit(size);

        // heap overflow (no fit found)
        if (prevBlock = MEM_END) {
            do Sys.error(6);
        }
        else {

            // bestFit returns null when the best block found is
            // at the top of freeList.
            if (prevBlock = null) {

                // foundBlock takes freeLists place
                let foundBlock = freeList;

                // allocate returns the block succeeding the block
                // being allocated in the freeList chain.
                let freeList = Memory.allocate(foundBlock, size);
            }

            // bestFit returns the block just preceding the actual
            // block to be allocated in the freeList chain. 
            else {

                // block to be allocated is next in list
                let foundBlock = prevBlock[FL_NEXT];

                // allocate returns the block succeeding the block
                // being allocated in the freeList chain.
                let prevBlock[FL_NEXT] = Memory.allocate(foundBlock, size);
            }
        }

        // return allocated block, accounting for header
        return foundBlock + 1;
    }

    // find the free memory block with the best fit
    function Array bestFit(int size) {
        var Array bestBlock, prevBlock, curBlock;
        var int bestSize, curSize;

        // default to end of memory, until match found
        let bestBlock = MEM_END;

        // we will try to minimize this value by searching
        let bestSize = MAX_SIZE;

        // start search at top of freeList
        let curBlock = freeList;
        let prevBlock = null;

        // search until end of list
        while( ~(curBlock = null)) {

            // number of usable words, accounting for block header
            let curSize = curBlock[FL_SIZE]-1;

            // fits & better than current bestSize
            if ( ~(curSize < size) & (curSize < bestSize)) {

                // new best fit
                let bestSize = curSize;

                // return the block before best fit block, to be
                // adjusted by allocate.
                let bestBlock = prevBlock;
            }

            // increment search along freeList chain
            let prevBlock = curBlock;
            let curBlock = curBlock[FL_NEXT];
        }

        return bestBlock;
    }

    // allocate the found block by adjusting freeList to exclude it
    function Array allocate(Array foundBlock, int size) {
        var Array nextBlock;
        var int blockSize;

        // foundBlock can fit block + block header + freeList header + more
        if (foundBlock[FL_SIZE] > (size + 1 + 2)) {

            // slice foundBlock into 2 pieces:
            //
            // [----------- foundBlock ------------]
            //  L->FL_SIZE, FL_NEXT
            //
            //              *chop*
            // [- allocated block -] [- nextBlock -]
            //  L->ALLOC_SIZE         L->FL_SIZE, FL_NEXT
            //
            let nextBlock = foundBlock + size + 1;

            // nextBlock continues the freeList chain, subtracting chopped size
            let nextBlock[FL_SIZE] = foundBlock[FL_SIZE] - (nextBlock - foundBlock);
            let nextBlock[FL_NEXT] = foundBlock[FL_NEXT];

            // create memory block, including size header
            let foundBlock = foundBlock + 1;
            let foundBlock[ALLOC_SIZE] = size + 1;
        }

        // entire foundBlock will be allocated
        else {

            // block size is entire block
            let blockSize = foundBlock[FL_SIZE];

            // next block is just the next block in the freeList chain
            let nextBlock = foundBlock[FL_NEXT];

            // create memory block, excluding header
            let foundBlock = foundBlock + 1;
            let foundBlock[ALLOC_SIZE] = blockSize;
        }

        // return the next block in the freeList chain
        return nextBlock;
    }

    /** De-allocates the given object (cast as an array) by making
     *  it available for future allocations. */
    function void deAlloc(Array object) {
        var int allocSize;
        var Array prevBlock;

        // get size of object from header
        let allocSize = object[ALLOC_SIZE];

        // subtract header from block
        let object = object - 1;

        // block is either before object in memory (lower) or null
        let prevBlock = Memory.findPrevFree(object);

        // no prevBlock found
        // [- all allocated -][- object -] ...
        if (prevBlock = null) {

            // object becomes new start of freeList
            // [- all allocated -][- freeList -] ...
            let object[FL_SIZE] = allocSize;
            let object[FL_NEXT] = freeList;
            let freeList = object;

            // new freed block becomes prevBlock, for defrag
            let prevBlock = object;
        }

        // block lower than object in memory found
        else {

            // block is right before object
            // [- prevBlock -][- object -] ...
            if ((prevBlock + prevBlock[FL_SIZE]) = object) {

                // fuse preceding block w/ object
                // [- prevBlock  (dead object) -] ...
                let prevBlock[FL_SIZE] = prevBlock[FL_SIZE] + allocSize;
            }

            // block is not right before object
            // [- prevBlock -][- stuff -][- object -] ...
            else {

                // link prevBlock to object, giving space back to freeList
                // [- prevBlock -][- stuff -][- free now -] ...
                //  |                             ^
                //  L- - - - - - - - - - - - - - /

                // object is now node in freeList
                let object[FL_SIZE] = allocSize;
                let object[FL_NEXT] = prevBlock[FL_NEXT];

                // prevBlock point to new node
                let prevBlock[FL_NEXT] = object;

                // new freed block becomes prevBlock, for defrag
                let prevBlock = object;
            }
        }

        // defrag
        do Memory.defrag(prevBlock);
        
		return;
    }

    // finds a free block of memory lower than object
    // if none can be found, returns null
    function Array findPrevFree(Array object) {
        var Array block;

        // start of freeList is above object in memory
        if (freeList > object) {
            return null;
        }

        // search through freeList until next is null or block is below object
        let block = freeList;
        while ( ~(block[FL_NEXT] = null) & (block[FL_NEXT] < object)) {
            let block = block[FL_NEXT];
        }
        return block;
    }

    // defragments the memory block, if possible
    function void defrag(Array prevBlock) {
        var Array nextBlock;

        // if prevBlock points to a free block right after it
        // [- prevBlock -][- nextBlock -]
        if ((prevBlock + prevBlock[FL_SIZE]) = prevBlock[FL_NEXT]) {

            // join two blocks together
            // [- new block -]
            let nextBlock = prevBlock[FL_NEXT];
            let prevBlock[FL_SIZE] = prevBlock[FL_SIZE] + nextBlock[FL_SIZE];
            let prevBlock[FL_NEXT] = nextBlock[FL_NEXT];
        }

        return;
    }
}
